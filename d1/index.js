let http = require("http");

// Mock Data

let courses = [

	{
		name : "English 101",
		price: 23000,
		isActive: true

	},
	{
		name: "Math 101",
		price: 25000,
		isActive: true
	},
	{
		name: "Reading 101",
		price: 21000,
		isActive: true
	}


]

http.createServer(function(request,response){

	// HTTP Methods allow us to identify what action to take for our request
	// With an HTTP Method we can actually routes that caters to the same endpoint but
	// with different action.
	// Most HTTP Methods are primarily concerned with CRUD operations
	
	// Common HTTP Methods:
	// GET = GET method request indicates that the client wants to retrieve or GET data
	
	// POST = POST method request indicates that the client wants to send data to create resource.
	
	// PUT = PUT method request indicates that the client wants to send data or updated resource
	
	// DELETE = DELETE method request indicates that the client wants to delete a resource.

	console.log(request.url);  //the request URL endpoint
	console.log(request.method);   //the method of the request

	if(request.url === "/" && request.method === "GET"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This a response to a GET Method request")
	
	}else if(request.url === "/" && request.method === "POST"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is a response to a POST method request!")
	
	}else if(request.url === "/" && request.method === "PUT"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This is a response to a PUT method request")
	
	}else if(request.url === "/" && request.method === "DELETE"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is a response to a DELETE method request")



}else if(request.url === "/courses" && request.method === "GET"){
		
		// Since we are now passing a stringified JSON, so that the client
		// which will receive our response be abe to display our data
		// properly,we have to update our Content-Type to application/json
		response.writeHead(200,{'Content-Type':'application/json'});
		// We cannot pass another data type other than a string for our end()
		// So, to be able to pass our array, we have to transform into JSON string.
		response.end(JSON.stringify(courses));

}else if(request.url === "/courses" && request.method === "POST"){
		
		// This route should be able to receive data from the client and we should be able to create
		// a new course and add it to our courses array.

		// This will act as a placeholder to contain the data passed from the client.
		let requestBody = "";

		// Receiving data from a client to a nodejs server requires 2 steps:

		// data step - this part will read the stream of data coming from our client and process
		// our client and process the incoming data into the requestBody variable.

		request.on('data',function(data){

			// console.log(data);  //stream of data from client
			requestBody += data // data stream is saved into the variable
		})

		// end step - this will run once or after the request data has been completely send from our client

		request.on('end',function(){
			

			console.log(requestBody); //requestBody now contains data from our Postman Client
			// since requestBody is JSON format we have to parse to add it as an object in our array
			requestBody = JSON.parse(requestBody);
			
			// Simulate creating document and adding it in a collection
			let newCourse = {
						

						//requestBody is now an object, we will get the name property of the 
						// requestBody as the value for the name of our newCourse object.Samw
						// with price/
				name: requestBody.name, 
				price: requestBody.price,
				isActive: true
			}

		console.log(newCourse);
		courses.push(newCourse);
		//check if the new courses added into the courses array.
		console.log(courses); 

		response.writeHead(200,{'Content-Type':'application/json'});
		// send the updated courses array to the client as a response
		response.end(JSON.stringify(courses));
		
		})



		
}

/*
	Mini Actiity
	Create a 2 new route for the following end point: "/courses"
	1. This route should be a "GET" method route
	-Add our status code, 200 , and our Content Type: text/plain
	-Add an end() method to end the response with the following message:
	"This is a GET method request for the / courses endpoint"

2. This route should be a "POST" method route
-Add out status code, 200 and our Content_Type: text/plain
-Add an end() method to end the response with the following message:
	"This is a GET method request for the / courses endpoint"

*/






}).listen(4000);

console.log("Server is running at localhost:4000");